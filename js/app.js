var todoApp = angular.module('todoApp', []);


todoApp.controller('TodoCtrl', ['$scope', 'filterFilter',
    function ($scope, filterFilter) {
        $scope.remaining = 0;
        $scope.saved = null;
        $scope.todos = [];
        if (localStorage.getItem('todos') !== null) {
            $scope.saved = JSON.parse(localStorage.getItem('todos'));
            angular.forEach($scope.saved, function (todo) {
                var oldTodo = {
                    name: '',
                    completed: false
                };
                oldTodo.name = todo.name;
                oldTodo.completed = todo.completed;
                $scope.todos.push(oldTodo);
            })
        } else {
            $scope.todos = [
                {
                    name: 'Tâche incomplète',
                    completed: false
                },
                {
                    name: 'Tâche complète',
                    completed: true
                }
            ];
        }


        $scope.removeTodo = function (index) {
            $scope.todos.splice(index, 1);
            localStorage.setItem('todos', JSON.stringify($scope.todos));
        };

        $scope.addTodo = function () {
            $scope.todos.push({
                name: $scope.newTodo,
                completed: false
            });
            localStorage.setItem('todos', JSON.stringify($scope.todos));
            $scope.newTodo = '';
        };

        $scope.$watch('todos', function () {
            $scope.remaining = filterFilter($scope.todos, {completed: false}).length;
        }, true);

        $scope.update = function() {
            localStorage.setItem('todos', JSON.stringify($scope.todos));
        }
    }]);
